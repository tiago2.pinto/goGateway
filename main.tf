provider "google" {
  credentials = file("google-service-account-key.json")
  project = "bold-syntax-354201"
  region = "us-central1"
  zone = "us-central1-a"
}

resource "google_cloud_run_service" "default" {
  name = "gogateway"
  location = "us-central1"


  template {
    spec {
      containers {
        image = "gcr.io/bold-syntax-354201/gogateway:latest"
        ports {
          container_port = 8080
        }
      }
    }
  }

  traffic {
    percent = 100
    latest_revision = true
  }
}